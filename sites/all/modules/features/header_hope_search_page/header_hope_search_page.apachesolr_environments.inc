<?php
/**
 * @file
 * header_hope_search_page.apachesolr_environments.inc
 */

/**
 * Implements hook_apachesolr_environments().
 */
function header_hope_search_page_apachesolr_environments() {
  $export = array();

  $environment = new stdClass();
  $environment->api_version = 1;
  $environment->env_id = 'solr';
  $environment->name = 'localhost server';
  $environment->url = 'http://212.115.233.102:8983/solr';
  $environment->service_class = '';
  $environment->conf = array();
  $environment->index_bundles = array(
    'node' => array(
      0 => 'article',
      1 => 'contacts',
      2 => 'event',
      3 => 'page',
      4 => 'person',
    ),
  );
  $export['solr'] = $environment;

  return $export;
}
