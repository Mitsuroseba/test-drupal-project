<?php
/**
 * @file
 * content_list_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_list_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function content_list_feature_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function content_list_feature_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: articles_list
  $nodequeues['articles_list'] = array(
    'qid' => '1',
    'name' => 'articles_list',
    'title' => 'Articles List',
    'subqueue_title' => '',
    'size' => '0',
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '0',
    'reference' => '0',
    'reverse' => '0',
    'i18n' => '0',
    'subqueues' => '1',
    'types' => array(
      0 => 'article',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: events_list
  $nodequeues['events_list'] = array(
    'qid' => '2',
    'name' => 'events_list',
    'title' => 'Events List',
    'subqueue_title' => '',
    'size' => '0',
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '0',
    'reference' => '0',
    'reverse' => '0',
    'i18n' => '0',
    'subqueues' => '1',
    'types' => array(
      0 => 'event',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: persons_list
  $nodequeues['persons_list'] = array(
    'qid' => '3',
    'name' => 'persons_list',
    'title' => 'Persons List',
    'subqueue_title' => '',
    'size' => '0',
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '0',
    'reference' => '0',
    'reverse' => '0',
    'i18n' => '0',
    'subqueues' => '1',
    'types' => array(
      0 => 'person',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}
