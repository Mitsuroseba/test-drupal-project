<div id="user_list_wrapper">
    <div style='border: 1px solid gray;'>Selected role: <?php print render($selected_role); ?></div>

    <table>
        <tr>
            <td>
              <?php print t('Username'); ?>
            </td>
            <td>
              <?php print t('Mail'); ?>
            </td>
            <td>
              <?php print t('Timezone'); ?>
            </td>
        </tr>
      <?php foreach ($users as &$user): ?>
        <tr>
            <td>
              <?php print render($user->name); ?>
            </td>
            <td>
              <?php print render($user->mail); ?>
            </td>

            <td>
              <?php print render($user->timezone); ?>
            </td>
        </tr>
      <?php endforeach; ?>
    </table>
</div>


