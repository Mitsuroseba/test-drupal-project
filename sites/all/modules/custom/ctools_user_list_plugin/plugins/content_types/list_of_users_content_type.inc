<?php
/**
 * @file
 * User list ctools content type.
 */

$plugin = array(
  'title' => t('List of users'),
  'content_types' => 'list_of_users_content_type',
  'single' => TRUE,
  'render callback' => 'list_of_users_content_type_render',
  'description' => t('List of all registered users'),
  'edit form' => 'list_of_users_content_type_edit_form',
  'admin title' => 'list_of_users_content_type_admin_title',
  'hook theme' => 'list_of_users_theme',
  'admin info' => 'list_of_users_content_type_admin_info',
  'category' => array(t('Service by Valera'), -9),
);

/**
 * Implementation admin title
 * @param $subtype
 * @param $conf
 * @param null $context
 * @return mixed|null|string
 */
function list_of_users_content_type_admin_title($subtype, $conf, $context = NULL) {
  $output = t('List of users');

  if ($conf['override_title'] && !empty($conf['override_title_text'])) {
    $output = filter_xss_admin($conf['override_title_text']);
  }

  return $output;
}

/**
 * Implements hook_admin_info
 */
function list_of_users_content_type_admin_info($subtype, $conf, $context) {
  $block = list_of_users_content_type_render($subtype, $conf, array("Some arg"), $context);
  return $block;
}

/**
 * Implementation of hook_theme().
 */
function list_of_users_theme(&$theme, $plugin) {
  $theme['users_list'] = array(
    'path' => $plugin['path'],
    'template' => 'users_list',
  );
  return $theme;
}

/**
 * Render function
 * @param $subtype
 * @param $conf
 * @param $args
 * @param $context
 * @return stdClass
 */
function list_of_users_content_type_render($subtype, $conf, $args, $context) {
  $auth_role_name = 'authenticated user';
  $block = new stdClass();
  $block->title = 'List of users';

  $roles = user_roles();
  $rid = $conf['selected_role'];

  $selected_role = $roles[$rid];
  $auth_role = user_role_load_by_name($auth_role_name);
  $query = db_select('users', 'u');
  $query->fields('u', array('uid', 'name', 'status', 'mail', 'timezone'));

  if ($rid != $auth_role->rid) {
    $query->innerJoin('users_roles', 'ur', 'u.uid = ur.uid');
    $query->condition('ur.rid', $rid);
  }

  $query->condition('u.status', 1);
  $query->orderBy('u.name', 'ASC');
  $query = $query->execute()
    ->fetchAllAssoc('uid');
  $block->content .= theme('users_list', array(
    'users' => $query,
    'selected_role' => $selected_role
  ));
  return $block;
}

/**
 * 'Edit' callback for the content type.
 */
function list_of_users_content_type_edit_form($form, &$form_state) {
  $roles = user_roles();
  $conf = $form_state['conf'];
  $form['selected_role'] = array(
    '#type' => 'select',
    '#title' => t('Select role:'),
    '#options' => $roles,
    '#default_value' => $conf['selected_role'],
    '#required' => TRUE,
  );
  return $form;
}

/**
 * Implementation hook_form_submit
 * @param $form
 * @param $form_state
 */
function list_of_users_content_type_edit_form_submit($form, &$form_state) {
  foreach (element_children($form) as $key) {
    if (!empty($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}
