<?php
/**
 * @file
 * Question machine ctools content type.
 *
 * This content type render question and answers
 *
 */

$plugin = array(
  'title' => t('Question machine'),
  'content_types' => 'question_machine_content_type',
  'single' => TRUE,
  'render callback' => 'question_machine_content_type_render',
  'description' => t('This content type render question and answers'),
  'admin title' => 'question_machine_content_type_admin_title',
  'hook theme' => 'question_machine_theme',
  'admin info' => 'question_machine_content_type_admin_info',
  'access callback' => TRUE,
  'category' => array(t('Service by Valera'), -9),
);

/**
 * Implementation admin title
 * @param $subtype
 * @param $conf
 * @param null $context
 * @return mixed|null|string
 */
function question_machine_content_type_admin_title($subtype, $conf, $context = NULL) {
  $output = t('Question machine');

  if ($conf['override_title'] && !empty($conf['override_title_text'])) {
    $output = filter_xss_admin($conf['override_title_text']);
  }

  return $output;
}

/**
 * Callback to provide administrative info (the preview in panels when building
 * a panel).
 *
 * In this case we'll render the content with a dummy argument and
 * a dummy context.
 */
function question_machine_content_type_admin_info($subtype, $conf, $context) {
  $block = question_machine_content_type_render($subtype, $conf, array("Some arg"), $context);
  return $block;
}


/**
 * Form builder; Build the Question machine content type form.
 */
function question_machine_content_type_form($form, &$form_state) {
  global $conf;
  $form['answers_set'] = array(
    '#type' => 'radios',
    '#title' => t('Choice answer'),
    '#default_value' => variable_get('comment_preview', 1),
    '#options' => $conf["questions_fieldset"]["question"],
  );

  $form['submit_answer'] = array(
    '#type' => 'submit',
    '#value' => t('Save answer and get results'),
    '#description' => t("Save answer and get results."),
    '#ajax' => array(
      'wrapper' => 'box_ajax_question',
      'path' => 'ajax_forms_ajax',
    ),
    '#weight' => 1,
  );
  return $form;
}


/**
 * Run-time rendering of the body of the block (content type)
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time
 * @param $args
 * @param $context
 *   Context - in this case we don't have any
 *
 * @return
 *   An object with at least title and content members
 */
function question_machine_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  $block->title = t('Question machine');
  $block->content .= t(variable_get('question_machine_question_text', 'Egg or hen ?'));
  $block->content .= drupal_render(drupal_get_form('question_machine_content_type_form'));
  return $block;
}

?>
