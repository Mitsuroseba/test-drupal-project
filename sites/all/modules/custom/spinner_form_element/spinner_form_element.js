(function ($, Drupal) {

    /**
     * This script adds jQuery slider functionality to spinner form element
     */
    Drupal.behaviors.spinner_form_element = {
        attach:function (context, settings) {
            // Create spinner
            $('.spinner-form-element', context).spinner()

        }
    }

})(jQuery, Drupal);