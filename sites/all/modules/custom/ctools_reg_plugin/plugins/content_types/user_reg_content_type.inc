<?php
/**
 * @file
 * Sample ctools content type that takes advantage of context.
 *
 * This example uses the context it gets (simplecontext) to demo how a
 * ctools content type can access and use context. Note that the simplecontext
 * can be either configured manually into a panel or it can be retrieved via
 * an argument.
 *
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
    'title' => t('Registration'),
    'content_types' => 'user_reg_content_type',
    'single' => TRUE,
    'render callback' => 'user_reg_content_type_render',
    'description' => t('Custom registration form'),
//    'edit form' => 'user_reg_content_type_edit_form',
    'admin title' => 'user_reg_content_type_admin_title',
    'hook theme' => 'user_reg_theme',
//    'admin info' => 'user_reg_content_type_admin_info',
    'category' => array(t('Registration by Misha'), -9),
);

function user_reg_content_type_render($subtype, $conf, $args, $context)
{
    $block = new stdClass();
    $block->title = 'Registration form';
    $block->content = $block->content = drupal_get_form('user_reg_form');

    return $block;
}

function user_reg_form($form, &$form_state)
{
    $form['user_reg_username'] = array(
        '#type' => 'textfield',
        '#title' => t('Username'),
        '#description' => t('Spaces are allowed; punctuation is not allowed except for periods, hyphens, apostrophes, and underscores.'),
        '#size' => 60,
        '#maxlength' => 60,
        '#required' => TRUE,
    );

    $form['user_reg_email'] = array(
        '#type' => 'textfield',
        '#title' => t('E-mail address'),
        '#description' => t('A valid e-mail address. All e-mails from the system will be sent to this address.
        The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail.'),
        '#size' => 60,
        '#maxlength' => 254,
        '#required' => TRUE,
    );

    $form['user_reg_pass'] = array(
        '#type' => 'password',
        '#title' => t('Password'),
        '#size' => 60,
        '#maxlength' => 60,
        '#required' => TRUE,
    );

    $form['user_reg_fname'] = array(
        '#type' => 'textfield',
        '#title' => t('First Name'),
        '#size' => 60,
        '#maxlength' => 60,
        '#required' => TRUE,
    );

    $form['user_reg_lname'] = array(
        '#type' => 'textfield',
        '#title' => t('Last Name'),
        '#size' => 60,
        '#maxlength' => 60,
        '#required' => TRUE,
    );


    $form['user_reg_gender'] = array(
        '#type' => 'select',
        '#title' => t('Gender'),
        '#options' => array(
            0 => t('Male'),
            1 => t('Female'),
        ),
        '#default_value' => t(''),
        '#required' => TRUE,
    );

    $form['user_reg_country'] = array(
        '#type' => 'textfield',
        '#title' => t('Country'),
        '#size' => 60,
        '#maxlength' => 60,
        '#required' => FALSE,
    );

    $form['user_reg_city'] = array(
        '#type' => 'textfield',
        '#title' => t('City'),
        '#size' => 60,
        '#maxlength' => 60,
        '#required' => FALSE,
    );

    $form['user_reg_spinner'] = array(
        '#type' => 'spinner_form_element',
        '#title' => t('Spinner'),
        '#required' => FALSE,
    );
  $form['user_reg_file'] = array(
    '#type' => 'frcs_file',
    '#required' => FALSE,
  );
// Submit button
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Register'),
    );

    return $form;
}



function user_reg_form_submit($form, &$form_state) {
    $roles = array(3 => true, 4 => true);

    $new_user = array(
        'name' => $form_state['values']['user_reg_username'],
        'pass' => $form_state['values']['user_reg_pass'],
        'mail' => $form_state['values']['user_reg_email'],
        'init' => $form_state['values']['user_reg_email'],
        'field_account_first_name' => array(LANGUAGE_NONE => array(array('value' => $form_state['values']['user_reg_fname']))),
        'field_account_last_name' => array(LANGUAGE_NONE => array(array('value' => $form_state['values']['user_reg_lname']))),
        'field_account_gender' => array(LANGUAGE_NONE => array(array('value' => $form_state['values']['user_reg_gender']))),
        'field_account_country' => array(LANGUAGE_NONE => array(array('value' => $form_state['values']['user_reg_country']))),
        'field_account_city' => array(LANGUAGE_NONE => array(array('value' => $form_state['values']['user_reg_city']))),
        'status' => 1,
        'access' => REQUEST_TIME,
        'roles' => $roles,
    );

    $account = user_save(null, $new_user);

    if ($account == false) {
        drupal_set_message(t('Noooo!'));
    } else {
        drupal_set_message(t('Yahoo!'));
    }
}

?>